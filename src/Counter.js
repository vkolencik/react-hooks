import React, {useEffect, useState} from "react";

export function Counter() {
  let [counter, setCounter] = useState(0);
  let [running, setRunning] = useState(true);
  let [intervalId, setIntervalId] = useState(null);

  useEffect(() => {
    document.title = `⏰${counter}`;
  });

  useEffect(() => {
    if (running && !intervalId) {
      setIntervalId(setInterval(() => setCounter(c => c + 1), 1000));
    }

    if (!running && intervalId) {
      clearInterval(intervalId);
      setIntervalId(null);
    }

    return () => {
      clearInterval(intervalId)
    }
  }, [running, intervalId]);

  return <>
    <output>{counter}</output>
    <div>
      <span onClick={() => setCounter(0)}>⏮</span>
      <span onClick={() => setCounter(c => c - 1)}>⏪</span>
      {running && <span onClick={() => setRunning(false)}>⏸</span>}
      {!running && <span onClick={() => setRunning(true)}>▶</span>}
      <span onClick={() => setCounter(c => c + 1)}>⏩</span>
    </div>
  </>
}