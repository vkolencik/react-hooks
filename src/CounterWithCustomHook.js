import React, {useEffect, useState} from "react";

function usePeriodicalAction(period, action) {
  let [isRunning, setRunning] = useState(true);
  let [intervalId, setIntervalId] = useState(null);

  useEffect(() => {
    if (isRunning && !intervalId) {
      setIntervalId(setInterval(action, period));
    }

    if (!isRunning && intervalId) {
      clearInterval(intervalId);
      setIntervalId(null);
    }

    return () => {
      clearInterval(intervalId)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isRunning, intervalId, period]);

  return [isRunning, () => {setRunning(true)}, () => {setRunning(false)}];
}

export function CounterWithCustomHook() {
  let [counter, setCounter] = useState(0);

  useEffect(() => {
    document.title = `⏰${counter}`;
  });

  let [isRunning, start, stop] = usePeriodicalAction(1000, () => setCounter(c => c + 1));

  return <>
    <output>{counter}</output>
    <div>
      <span onClick={() => setCounter(0)}>⏮</span>
      <span onClick={() => setCounter(c => c - 1)}>⏪</span>
      {isRunning && <span onClick={stop}>⏸</span>}
      {!isRunning && <span onClick={start}>▶</span>}
      <span onClick={() => setCounter(c => c + 1)}>⏩</span>
    </div>
  </>
}