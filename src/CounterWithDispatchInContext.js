import React, {useContext, useEffect, useReducer} from "react";

function reducer(state, action) {
  switch (action.type) {
    case 'increment':
      return {...state, counter: state.counter + 1};
    case 'decrement':
      return {...state, counter: state.counter - 1};
    case 'reset':
      return {...state, counter: 0};
    case 'start':
      return {...state, running: true};
    case 'stop':
      return {...state, running: false};
    case 'schedule':
      return {...state, intervalId: action.intervalId};
    case 'unschedule':
      return {...state, intervalId: null};
    default:
      throw new Error(`Unknown action type: ${action.type}`);
  }
}

const DispatchContext = React.createContext(null);

export function CounterWithDispatchContext() {
  const [state, dispatch] = useReducer(reducer, {running: true, counter: 0, intervalId: null})

  useEffect(() => {
    document.title = `⌚${state.counter}`;
  });

  useEffect(() => {
    if (state.running && !state.intervalId) {
      dispatch({type: 'schedule', intervalId: setInterval(() => dispatch({type:'increment'}), 1000)});
    }

    if (!state.running && state.intervalId) {
      clearInterval(state.intervalId);
      dispatch({type: 'unschedule'});
    }

    return () => {
      clearInterval(state.intervalId)
    }
  }, [state.running, state.intervalId]);

  return <DispatchContext.Provider value={dispatch}>
    <output>{state.counter}</output>
    <div>
      <ResetButton/>
      <DecrementButton/>
      {state.running && <StopButton/>}
      {!state.running && <StartButton/>}
      <IncrementButton/>
    </div>
  </DispatchContext.Provider>
}

function ResetButton() {
  const dispatch = useContext(DispatchContext);
  return <span onClick={() => dispatch({type: 'reset'})}>⏮</span>
}

function DecrementButton() {
  const dispatch = useContext(DispatchContext);
  return <span onClick={() => dispatch({type: 'decrement'})}>⏪</span>
}

function StopButton() {
  const dispatch = useContext(DispatchContext);
  return <span onClick={() => dispatch({type: 'stop'})}>⏸</span>
}

function StartButton() {
  const dispatch = useContext(DispatchContext);
  return <span onClick={() => dispatch({type: 'start'})}>▶</span>
}

function IncrementButton() {
  const dispatch = useContext(DispatchContext);
  return <span onClick={() => dispatch({type: 'increment'})}>⏩</span>
}
