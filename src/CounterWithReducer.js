import React, {useEffect, useReducer} from "react";

function reducer(state, action) {
  switch (action.type) {
    case 'increment':
      return {...state, counter: state.counter + 1};
    case 'decrement':
      return {...state, counter: state.counter - 1};
    case 'reset':
      return {...state, counter: 0};
    case 'start':
      return {...state, running: true};
    case 'stop':
      return {...state, running: false};
    case 'schedule':
      return {...state, intervalId: action.intervalId};
    case 'unschedule':
      return {...state, intervalId: null};
    default:
      throw new Error(`Unknown action type: ${action.type}`);
  }
}

export function CounterWithReducer() {
  const [state, dispatch] = useReducer(reducer, {running: true, counter: 0, intervalId: null})

  useEffect(() => {
    document.title = `⌚${state.counter}`;
  });

  useEffect(() => {
    if (state.running && !state.intervalId) {
      dispatch({type: 'schedule', intervalId: setInterval(() => dispatch({type:'increment'}), 1000)});
    }

    if (!state.running && state.intervalId) {
      clearInterval(state.intervalId);
      dispatch({type: 'unschedule'});
    }

    return () => {
      clearInterval(state.intervalId)
    }
  }, [state.running, state.intervalId]);

  return <>
    <output>{state.counter}</output>
    <div>
      <span onClick={() => dispatch({type: 'reset'})}>⏮</span>
      <span onClick={() => dispatch({type: 'decrement'})}>⏪</span>
      {state.running && <span onClick={() => dispatch({type: 'stop'})}>⏸</span>}
      {!state.running && <span onClick={() => dispatch({type: 'start'})}>▶</span>}
      <span onClick={() => dispatch({type: 'increment'})}>⏩</span>
    </div>
  </>
}